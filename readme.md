# Notes

- document ready bei mount s. button
- einheitliche form der doku
- jquery
- bootstrap
- kein vendor prefix
- scss mit bem
- git repo in dotsource
- TODO LM: was ist noch zu tun
- js- klassen an elemente
- css variables (farbthemes)
- mobile folgt
- pages erst am ende
- sections (wie config)
- allgemein wie arbeitet man
- style vom guide noch in progress


# Theme

The frontend is compiled via npm scripts. They can be found in the package.json under "scripts".
To run the scripts, you have to open a new terminal window and execute them in the project root directory.

## Requirements

- nodejs ^8.14.0  
- npm ^6.4.1

## Initialization

Before using Webpack or if you change the package.json dependencies, you have to run `npm install` once.
This command downloads the dependencies from the package.json and saves them to the /node_modules folder.
When compiling with Webpack, all required JavaScript dependencies are integrated into the JavaScript output file.
You do not need to add the /node_modules folder to git and it can be deleted after compilation.

## Webpack

All theme-specific resources are compiled via webpack.

There are several entry points:
- src/main.js (including /scss/main.scss)
- webresources/scss/bootstrap-grid.scss

The compiled files are created in the folder /webresources/dist/.

#### Available Scripts:

- `npm run styleguide:dev` Creates the styleguide and starts a node.js server with hot update
- `npm run styleguide:prod` Creates the styleguide under dist/styleguide

- `npm run build:dev` Simple compilation with source maps and linting for development phase
- `npm run build:prod` Advanced compilation with minification and uglyfication for production systems

Additionally there are scripts that extend their parent's build process by a subsequent watch process:

- `npm run build:dev:watch` and `npm run build:prod:watch` 

#### Best practice:

Development

1. Navigate to the project's root
2. Install the npm dependencies: `npm install`
3. Start the server: `npm run styleguide:dev`
4. CTRL + C to end the process and terminate the server

Production

1. Navigate to the project's root
2. Install the npm dependencies: `npm install`
3. Build the frontend: `npm run styleguide:prod`

## Adding a npm package

1. Search for the library: https://www.npmjs.com/
2. See "Install" section on the package's detail page (e.g. `npm i bootstrap`)
3. (a) If the package needs to be in the main.build.js: Add `--save` to the install command
(b) If the package is only used during the build process: Add `--save-dev` to the install command
4. Make sure the package is now listed in the package.json (dependencies or devDependencies)
5. Commit the package.json and package-lock.json to git

**Note:** If the package.json changes and the package is not in your node_modules folder, you need to execute `npm install` once.