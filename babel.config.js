module.exports = {
    presets: ['@babel/preset-env'],
    plugins: ['@babel/plugin-transform-runtime', '@babel/plugin-transform-react-jsx', '@babel/plugin-transform-modules-commonjs'],
    comments: false
};