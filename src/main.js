require("@babel/polyfill");

// Styles
// -----------------------------------
require('./scss/main.scss');

// Libraries
// -----------------------------------
require('bootstrap');

// Custom resources
// -----------------------------------
const Accordion = require('./js/accordion');

// Export LM objects for global usage
// -----------------------------------
module.exports = {
  Accordion: Accordion
};