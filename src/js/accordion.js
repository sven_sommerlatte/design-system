const Accordion = {
  init() {
    console.log('accordion initialized');

    console.log($('.accordion-test'));

    this.handleClick();
  },
  handleClick() {
    $('.js-accordion').off('click').on('click', () => {
      console.log('accordion clicked');
    });
  }
};

$(document).ready(function () {
  Accordion.init();
});

module.exports = Accordion;