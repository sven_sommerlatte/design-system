const webpack = require('webpack');
const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  entry: {
    main: './src/main.js'
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
    library: 'LM'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {}
        }
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(jpe?g|gif|png|svg)(\?[a-z0-9=.]+)?$/,
        loader: 'url-loader',
        options: {
          limit: 20000,
          name: 'images/[name]-[contenthash].[ext]',
          publicPath: '../'
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)(\?[a-z0-9=.]+)?$/,
        loader: 'url-loader',
        options: {
          limit: 20000,
          name: 'fonts/[name]-[contenthash].[ext]',
          publicPath: '../'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true
  },
  performance: {
    hints: false
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.ProvidePlugin({
      $: "jquery"
    })
  ].concat(
    process.argv.includes('--analyze') ? [new BundleAnalyzerPlugin()] : []
  ),
  watchOptions: {
    aggregateTimeout: 300,
    ignored: /node_modules/
  },
  node: {
    fs: 'empty'
  }
};