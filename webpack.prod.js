const merge = require('webpack-merge');
const commonConfig = require('./webpack.common.js');
const path = require('path');
const dist_path = __dirname + '/dist';

// Plugins
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');

module.exports = merge(commonConfig, {
  output: {
    filename: '[name]-[contenthash].js'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: false
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: false
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([dist_path]),
    new MiniCssExtractPlugin({
      filename: 'css/[name]-[contenthash].css',
      allChunks: true
    }),
    new CopyWebpackPlugin([
      {from: 'public/'}
    ]),
    new WorkboxPlugin.GenerateSW({
      importScripts: ['/push-utils.js', '/sw-inject.js'],
      navigateFallback: '/index.html',
      skipWaiting: true,
      clientsClaim: true,
      cleanupOutdatedCaches: true,
      exclude: [
        /\.(?:png|jpg|jpeg|gif|svg|css|htaccess|php|html)$/,
        /^(?!service-worker\.js$).+\.(?:js)$/m,
        'avatar/**',
        'car-src/**',
        'clubkits/**',
        'playerimage-src/**'
      ],
      runtimeCaching: [{
        urlPattern: new RegExp('^(?!.*(cronjobs/|api/|avatar/|car-src/|clubkits/|playerimage-src/|collect\\?|analytics\.js)).*$'),
        handler: 'NetworkFirst'
      }]
    })
  ],
  devtool: '(none)',
  watch: false
});