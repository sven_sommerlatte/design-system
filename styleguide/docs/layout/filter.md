## Vorkommen
- Ausstellerverzeichnis
- Produktverzeichnis

## Ausprägungen
- Suchbegriff
- A-Z, Radio, Checkbox, Input, Select
- Aktive Filter

## Mobile
- Suchbegriff + Open-Button
- Öffnen in Slideup
- in Akkordion
- aktive filter -> truncate