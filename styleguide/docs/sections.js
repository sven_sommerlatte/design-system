module.exports = [
    {
        name: 'Base',
        sectionDepth: 1,
        pagePerSection: true,
        sections: [
            {
                name: 'Colors',
                content: './styleguide/docs/base/colors.md',
                exampleMode: 'hide'
            },
            {
                name: 'Icons'
            },
            {
                name: 'Typography'
            },
            {
                name: 'Various'
            }
        ]
    },
    {
        name: 'Components',
        sectionDepth: 1,
        pagePerSection: true,
        sections: [
            {
                name: 'Accordion',
                content: './styleguide/docs/components/accordion.md'
            },
            {
                name: 'Buttons'
            },
            {
                name: 'Cards'
            },
            {
                name: 'Image Gallery'
            },
            {
                name: 'Images'
            },
            {
                name: 'Links'
            },
            {
                name: 'Logos'
            },
            {
                name: 'Pagination'
            },
            {
                name: 'Pop Up'
            },
            {
                name: 'Section Headline'
            },
            {
                name: 'Show More'
            },
            {
                name: 'Slider'
            },
            {
                name: 'Truncate Text'
            }
        ]
    },
    {
        name: 'Layout',
        sectionDepth: 1,
        pagePerSection: true,
        sections: [
            {
                name: 'Company Contact',
                content: './styleguide/docs/layout/company-contact.md'
            },
            {
                name: 'Event Footer'
            },
            {
                name: 'Event Header'
            },
            {
                name: 'Filter'
            },
            {
                name: 'Footer'
            },
            {
                name: 'Header'
            },
            {
                name: 'Navigation'
            },
            {
                name: 'News Article'
            },
            {
                name: 'Person Card'
            },
            {
                name: 'Product Breadcrumbs'
            },
            {
                name: 'Product Portfolio'
            },
            {
                name: 'Side Bar'
            },
            {
                name: 'Social Bar'
            },
            {
                name: 'Sorting'
            },
            {
                name: 'Speaker'
            },
            {
                name: 'Sticky Content'
            },
            {
                name: 'Teaser'
            },
            {
                name: 'Ticket Prices'
            }
        ]
    },
    {
        name: 'Pages',
        content: './styleguide/docs/pages.md',
        pagePerSection: true,
        sectionDepth: 1
    }
];