## Typography

``` vue
const Vue = require('vue').default
const Typography = require('./typography.vue').default
Vue.component('Typography', Typography)

<Typography />
```

<!--
https://patternlab-handlebars-preview.netlify.com/?p=viewall-atoms-text

headings
paragraph
blockquote
inline elements
-->