# Beispiel 1

<!-- Inhalt wird direkt gerendert, Vue SFC -->
<!-- noeditor to hide editor container -->

```vue
<template>
<div>
    <div class="accordion js-accordion" v-for="index in 5">
        <div class="accordion__title js-accordion-trigger">
            TITLE
            
            <div class="accordion__title__icon accordion__title__icon--red">O</div>
        </div>
        <div class="accordion__body">
            ich bin nur ein test 
            <span style="background: red">auf rotem hintergrund</span>
        </div>
    </div>
</div>
</template>

<script>
    const Accordion = require('../../../src/js/accordion');

    export default {
        mounted() {
            Accordion.init();
        }
    }
</script>
```

<!-- Bereinigte Dokumentation -->

```html
<div class="accordion-test">
    ich bin nur ein test 
    <!-- TODO LM: add text -->
    <span style="background: red">auf rotem hintergrund</span>
</div>
...
```

# Beispiel 2

```vue noeditor
<div class="accordion-test" style="border: 1px solid red">
    ein weiterer test
</div>
```

<!-- static will not be rendered -->

```js static
// Auch JS kann dokumentiert werden
var foo = 'bar';
```

# Notes

## Vorkommen
- Produktgruppen
- Ticket Preisübersicht
- Tab Element

## Ausprägungen
- title 
- title slot
- content slot

## Mobile