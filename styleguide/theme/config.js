// https://github.com/styleguidist/react-styleguidist/blob/master/src/client/styles/theme.ts

const spaceFactor = 10;

const config = {
    borderRadius: 0,
    maxWidth: 'auto',
    sidebarWidth: 220,
    space: [
        spaceFactor / 2,
        spaceFactor,
        spaceFactor * 2,
        spaceFactor * 3,
        spaceFactor * 4,
        spaceFactor * 5,
        spaceFactor * 6,
    ],
    fontFamily: {
        base: ['Helvetica', 'Arial', 'sans-serif'],
        monospace: ['Consolas', "'Liberation Mono'", 'Menlo', 'monospace']
    },
    fontSize: {
        base: 15,
        text: 16,
        small: 13,
        h1: 48,
        h2: 36,
        h3: 24,
        h4: 18,
        h5: 16,
        h6: 16
    }
};

module.exports = {
    theme: config,
    styles: {
        StyleGuide: {
            root: {
                'background': 'var(--bg-base)',
                'color': 'var(--color-base)'
            },
            logo: {
                isolated: false,
                'padding': 0,
                'color': 'var(--color-base)',
                'border': 'none'
            },
            sidebar: {
                'border-right': '1px solid var(--bg-alt)',
                'background': 'var(--bg-base)'
            },
            footer: {
                'display': 'none'
            }
        },
        Props: {
            type: {
                'margin': '0',
                'opacity': '1',
                '& pre': {
                    'margin': 0
                }
            },
            descriptionWrapper: {
                '& p': {
                    'margin-bottom': '10px'
                }
            }
        },
        Version: {
            version: {
                isolated: false,
                'display': 'none'
            }
        },
        Logo: {
            logo: {
                isolated: false,
                'color': 'var(--color-base)',
                'font-weight': 700,
                'text-align': 'left',
                'text-transform': 'uppercase',
                'font-size': '16px',
                'white-space': 'nowrap'
            }
        },
        Link: {
            link: {
                '&, &:link, &:visited': {
                    isolated: false,
                    'font-weight': 800,
                    'color': 'var(--color-accent)',
                    'text-transform': 'uppercase',
                    'font-size': '11px',
                    'padding': '0 10px',
                    'margin-bottom': '5px',
                    'display': 'block'
                },
                '&:hover': {
                    isolate: false,
                    'color': 'var(--color-base)'
                }
            }
        },
        ComponentsList: {
            list: {
                '&': {
                    'padding': '0 10px'
                },
                '& &': {
                    'padding': '5px 0 0',
                    'margin': '0 0 25px'
                },
                '& & a': {
                    isolate: false,
                    'font-weight': 400,
                    'color': 'var(--color-base)',
                    'text-transform': 'none',
                    'font-size': '14px',
                    'display': 'block',
                    'padding': '6px 10px',
                    'margin-bottom': 0,
                },
                '& & a:hover': {
                    isolate: false,
                    'color': 'var(--color-base)'
                }
            },
            item: {
                'margin': '0'
            },
            isSelected: {
                'background': 'var(--bg-alt)',
                'border-radius': '4px',
                '& > a': {
                    'color': 'var(--color-base) !important'
                }
            },
            heading: {
                'cursor': 'text !important',
                'pointer-events': 'none !important'
            }
        },
        TableOfContents: {
            search: {
                'padding': '0 20px 15px',
                'display': 'none'
            },
            input: {
                isolated: false,
                'padding': '0 10px',
                'height': '30px',
                'font-size': '14px',
                'color': 'var(--color-base)',
                'border': '2px solid transparent',
                'background': 'var(--bg-alt)',
                'border-radius': '4px',
                '&::placeholder': {
                    'color': 'var(--color-light)',
                    'font-size': '14px'
                },
                '&:focus': {
                    'border-color': 'var(--color-base)',
                    'background': 'transparent',
                    'box-shadow': 'none'
                }
            }
        },
        Playground: {
            preview: {
                isolated: false,
                'padding': '20px 0',
                'background-color': 'transparent',
                'border': 'none',
                'margin': '0 0 10px',
                'border-radius': '0'
            },
            controls: {
                'display': 'none'
            },
            tab: {
                '& textarea, & pre': {
                    'font-size': '15px !important',
                    'padding': '20px !important',
                    'line-height': '1.2 !important'
                }
            }
        },
        SectionHeading: {
            sectionName: {
                'line-height': 1.3,
                'font-size': '38px',
                'font-weight': 800,
                'text-transform': 'uppercase',
                '&:hover': {
                    'line-height': 1.3,
                    'text-decoration': 'none',
                    'font-size': '38px',
                    'font-weight': 800,
                    'text-transform': 'uppercase'
                }
            },
            wrapper: {
                'margin': '0 0 50px',
                '& > h1': {
                    margin: '0'
                }
            },
            toolbar: {
                display: 'none'
            }
        },
        Editor: {
            root: {
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0,
                '& textarea': {
                    'border': 'none !important',
                }
            },
            jssEditor: {
                'background': 'var(--code-background)',
                'border-radius': '4px'
            }
        },
        Table: {
            cellHeading: {
                'color': 'var(--color-base)',
                'padding': '15px 10px',
                'text-transform': 'uppercase',
                'font-size': '12px',
                'font-weight': 600
            },
            tableHead: {
                'background': 'var(--bg-table-head)',
                'border-bottom': '1px solid var(--bg-body)'
            },
            cell: {
                'padding': '15px 45px 15px 10px',
                'font-size': '14px',
                'border-bottom': '1px solid var(--bg-body)',
                'color': 'var(--color-base)'
            },
            table: {
                'margin-bottom': '10px',
                'background': 'var(--bg-table)',
                '& tbody tr:hover': {
                    'background-color': 'var(--bg-table-hover)'
                }
            }
        },
        Heading: {
            heading1: {
                'font-size': '28px',
                'font-weight': 500,
                'color': 'var(--color-base)'
            },
            heading2: {
                'font-size': '24px',
                'font-weight': 500,
                'color': 'var(--color-base)'
            },
            heading3: {
                'font-size': '20px',
                'font-weight': 500,
                'color': 'var(--color-base)'
            },
            heading4: {
                'font-size': '16px',
                'font-weight': 500,
                'color': 'var(--color-base)'
            },
            heading5: {
                'font-size': '14px',
                'font-weight': 500,
                'color': 'var(--color-base)'
            }
        },
        tabs: {
            'margin-bottom': '100px'
        },
        TabButton: {
            button: {
                'border': 'none !important',
                'font-size': '24px',
                'text-transform': 'none',
                'padding': 0,
                'margin-bottom': '20px',
                'color': 'var(--color-base)',
                'font-weight': 500,
                'cursor': 'text',
                'pointer-events': 'none',
                'transition': 'none'
            }
        },
        ReactComponent: {
            header: {
                'margin-bottom': '50px'
            },
            docs: {
                'margin-bottom': '50px'
            },
            tabs: {
                'margin-bottom': '50px'
            },
            tabButtons: {
                'margin-bottom': 0
            }
        },
        Name: {
            name: {
                'font-size': '14px',
                'color': 'var(--color-base) !important',
            }
        },
        Type: {
            type: {
                'font-size': '14px',
                '& pre': {
                    'color': 'var(--color-base) !important'
                }
            }
        },
        Code: {
            code: {
                'font-size': '14px'
            }
        },
        Para: {
            para: {
                'color': 'var(--color-light)',
                'font-size': '16px !important',
                'table &': {
                    'font-size': '16px !important',
                    'color': 'var(--color-light)'
                }
            }
        },
        Pathline: {
            pathline: {
                'display': 'none'
            }
        },
        Pre: {
            pre: {
                'background': 'var(--code-background)',
                'border': 'none',
                'padding': '20px 25px',
                'font-size': '14px !important',
                'line-height': '1.4 !important',
                'border-radius': '4px'
            },
            token: {
                '&.tag': {
                    'color': 'var(--code-tag)'
                }
            }
        }
    }
};