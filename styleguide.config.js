const path = require('path');
const sections = require('./styleguide/docs/sections');

module.exports = {
  title: 'Leipziger Messe',
  version: require('./package.json').version,
  pagePerSection: true,
  simpleEditor: true,
  template: {
    title: 'Leipziger Messe - Design System',
    lang: 'de',
    trimWhitespace: true,
    head: {
      meta: [
        {
          name: 'viewport',
          content: 'width=device-width,initial-scale=1.0'
        },
        {
          name: 'format-detection',
          content: 'telephone=no'
        }
      ]
    }
  },
  getComponentPathLine(componentPath) {
  },
  ...require('./styleguide/theme/config'),
  webpackConfig: require('./webpack.config'),
  require: [
    path.join(__dirname, 'styleguide/main.js')
  ],
  usageMode: 'expand',
  exampleMode: 'expand',
  styleguideDir: 'dist/styleguide',
  sections: sections
};